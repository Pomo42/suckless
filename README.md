# My suckless build
This is my suckless build that I use on my desktop, designed to work using
[Gentoo's portage](https://www.gentoo.org/) and [the savedconfig use
flag](https://wiki.gentoo.org/wiki/Savedconfig). This may or may not work with
other distributions, I will only be testing for Gentoo. Dwm, dmenu and st are
NOT made by me. The original source code can be found at https://suckless.org.

# Installation
All the commands should be ran as root user and you are in the git repository
directory.

1. Add the savedconfig use flag to `x11-wm/dwm`, `x11-misc/dmenu` and `x11-terms/st`
```{sh}
echo "x11-wm/dwm savedconfig
x11-misc/dmenu savedconfig
x11-terms/st savedconfig" > /etc/portage/package.use/suckless
```
If you are using multi monitors, make sure to add the xinerama use flag to dwm
and dmenu.

2. Move the `savedconfig` directory to `/etc/portage`
```{sh}
cp -r savedconfig /etc/portage/
```

3. Move the `patches` directory to `/etc/portage`
```{sh}
cp -r patches /etc/portage/
```

4. Emerge `x11-wm/dwm`, `x11-misc/dmenu` and `x11-terms/st`
```{sh}
emerge x11-wm/dwm x11-misc/dmenu x11-terms/st
```

5. Dwm, dmenu and st should now be installed and you may use these how you like
   (e.g. adding to an xinitrc file or setting up with a display manager).

# Features
## Dwm
- Support for tiled and monocle layout
- Purple colour scheme
- Terminal swallowing
## Dmenu
- Centred position (like [rofi](https://github.com/davatorium/rofi))
- Fits the colour scheme of the dwm build
## St
- Scrollback
- X cursor hides while typing
- Gruvbox colour scheme

# Dwm keybinds
| Key             | Action                        |
|-----------------|-------------------------------|
| win+d           | dmenu                         |
| win+shift+enter | st                            |
| win+a           | pulsemixer                    |
| win+s           | Screenshot                    |
| win+r           | Calculator (requires bc)      |
| win+v           | htop                          |
| win+b           | Toggle bar                    |
| win+(j/k)       | Change window focus           |
| win+(h/l)       | Change master width           |
| win+enter       | Move focused window to master |
| win+c           | Kill focused window           |
| win+space       | Change layout                 |
| win+shift+space | Toggle floating               |
| win+(1-5)       | Change to workspace           |
| win+shift+(1-5) | Move window to workspace      |
